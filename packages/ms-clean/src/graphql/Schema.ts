import { gql } from "apollo-server";

export const typeDefs = gql`
  type Library {
    branch: String!
    books: [Book!]
  }

  type Book {
    title: String!
  }

  type Query {
    libraries: [Library]
  }
  type Mutation {
    addLibrary(branch: String): Library
    addBook(libraryBranch: String, bookTitle: String): Book
  }
`;
