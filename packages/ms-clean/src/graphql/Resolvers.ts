import { createBook } from "../usecases/CreateBook";
import { libraryRepository, bookRepository } from "../database";
import { createLibrary } from '../usecases';

const libraryRepositoryInstance = libraryRepository([]);
const bookRepositoryInstance = bookRepository([]);

export const resolvers = {
  Query: {
    libraries() {
      return libraryRepositoryInstance.getAllLibraries();
    },
  },
  Mutation: {
    addLibrary(parent: any, args: any, context: any, info: any) {
      return createLibrary(libraryRepositoryInstance)(args);
    },
    addBook(parent: any, args: any, context: any, info: any) {
      return createBook(bookRepositoryInstance)(libraryRepositoryInstance)(args);
    },
  },
  Library: {
    books(parent: any) {
      return bookRepositoryInstance.getBooksByBranch(parent.branch);
    },
  },
};
