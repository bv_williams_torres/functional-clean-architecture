import * as R from "ramda";
import { Library } from "../models";
import { LibraryRepository } from "../repository";

export const libraryRepository = (libraries: Library[]): LibraryRepository => {
  return {
    saveLibrary: (library: Library) => {
      const newLibrary = R.clone(library);
      libraries.push(newLibrary);
      return newLibrary;
    },
    getAllLibraries: () => libraries,
    getLibrary: (branch: string) =>
      libraries.find((library) => library.branch === branch),
  };
};
