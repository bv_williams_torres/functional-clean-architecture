import * as R from "ramda";
import { Book } from "../models";
import { BookRepository } from "../repository";

export const bookRepository = (books: Book[]): BookRepository => {
  return {
    saveBook: (book: Book) => {
      const newBook = R.clone(book);
      books.push(newBook);
      return newBook;
    },
    getBooksByBranch: (branch: string) =>
      books.filter((book) => book.branch === branch),
  };
};
