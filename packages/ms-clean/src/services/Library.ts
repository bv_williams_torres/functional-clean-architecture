import * as Joi from "joi";
import { Library } from "../models";

const librarySchema = Joi.object({
  branch: Joi.string().alphanum().min(3).max(30).required(),
});

export const librarySchemaPolicy = async (library: Library) =>
  librarySchema.validateAsync(library);
