import * as Joi from "joi";
import { Book } from "../models";
import { LibraryRepository } from "../repository";

const bookSchema = Joi.object({
  title: Joi.string().min(3).max(30).required(),
  branch: Joi.string().alphanum().min(3).max(30).required(),
});

export const bookMapper = (book: any) => ({
  branch: book.libraryBranch,
  title: book.bookTitle,
});

export const bookSchemaPolicy = (book: Book): Promise<Book> => bookSchema.validateAsync(book);

export const existLibraryPolicy = (
  libraryRepository: LibraryRepository,
  book: Book
): Promise<Book> =>
  new Promise((resolve, reject) => {
    libraryRepository.getLibrary(book.branch)
      ? resolve(book)
      : reject(`The library "${book.branch}" does not exist`);
  });
