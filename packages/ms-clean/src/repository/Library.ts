import { Library } from "../models";

export type LibraryRepository = {
  saveLibrary: (newLibrary: Library) => Library;
  getAllLibraries: () => Library[];
  getLibrary: (branch: string) => Library;
};
