import { Book } from "../models";

export type BookRepository = {
  saveBook: (book: Book) => Book;
  getBooksByBranch: (branch: string) => Book[];
};
