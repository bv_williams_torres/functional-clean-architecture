export type Book = {
  title: string
  branch: string
}