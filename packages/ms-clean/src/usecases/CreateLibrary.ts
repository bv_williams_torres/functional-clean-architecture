import * as R from "ramda";
import { librarySchemaPolicy } from "../services";
import { LibraryRepository } from "../repository";

export const createLibrary = (repository: LibraryRepository) => R.pipe(librarySchemaPolicy, R.andThen(repository.saveLibrary));
