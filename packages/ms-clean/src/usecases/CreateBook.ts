import * as R from "ramda";
import { bookMapper, bookSchemaPolicy, existLibraryPolicy } from "../services";
import { BookRepository, LibraryRepository } from "../repository";

export const createBook = (bookRepository: BookRepository) => (libraryRepository: LibraryRepository) => {
  const existLibraryPolicyCurried = R.curry(existLibraryPolicy)(libraryRepository);
  return R.pipe(
    bookMapper,
    bookSchemaPolicy,
    R.andThen(existLibraryPolicyCurried),
    R.andThen(bookRepository.saveBook)
  );
}
