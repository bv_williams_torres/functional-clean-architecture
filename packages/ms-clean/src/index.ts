import { ApolloServer, gql } from "apollo-server";
import { resolvers, typeDefs } from "./graphql";

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url} ${process.env.npm_package_version}`);
});
